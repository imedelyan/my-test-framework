//
//  EKModalStatus.h
//  EKModalStatus
//
//  Created by Igor Medelyan on 5/31/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for EKModalStatus.
FOUNDATION_EXPORT double EKModalStatusVersionNumber;

//! Project version string for EKModalStatus.
FOUNDATION_EXPORT const unsigned char EKModalStatusVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <EKModalStatus/PublicHeader.h>


